# -*- coding: utf-8 -*-
"""
Created on Wed May 31 11:56:48 2023

@author: wiesbrock
"""

# -*- coding: utf-8 -*-
"""
Created on Wed May  3 09:33:43 2023

@author: wiesbrock
"""

from IPython import get_ipython
#get_ipython().magic('reset -sf')
# Turn interactive plotting off


import numpy as np
import glob
import matplotlib.pyplot as plt
import pandas as pd
import os
import scipy.stats as stats
import seaborn as sns
from scipy.signal import argrelextrema
from tqdm import tqdm
from read_roi import read_roi_file
import math

def distance(x1, y1, x2, y2):
    return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)

def crosscorr(datax, datay, lag=0):
    """ Lag-N cross correlation. 
    Parameters
    ----------
    lag : int, default 0
    datax, datay : pandas.Series objects of equal length
    Returns
    ----------
    crosscorr : float
    """
    return datax.corr(datay.shift(lag))

def zero_sum_cols(arr):
    """
    Gibt eine Liste zurück, die den Index der Spalte angibt, welche Spalten eines Arrays eine Summe von 0 ergeben.
    
    Args:
    arr (list): Eine zweidimensionale Liste, die das Array darstellt.
    
    Returns:
    Eine Liste mit den Indizes der Spalten, die eine Summe von 0 ergeben.
    """
    num_cols = len(arr[0])
    result = []
    
    for j in range(num_cols):
        col_sum = sum(row[j] for row in arr)
        if col_sum == 0:
            result.append(j)
    
    return result

def extract_numbers(s):
    return tuple(int(x) if x.isdigit() else x for x in re.split('(\d+)', s))

path=r'C:\Users\wiesbrock\Desktop\AMH_FSH_systemic/'
search=path+'*'
folder_list=glob.glob(search)
for i in range(len(folder_list)):
    folder_list[i]=os.path.normpath(folder_list[i])

maximum_lag=60
all_cross_corr=[]
all_index=[]
all_high_distance=[]
all_low_distance=[]
list_active=[]
import re
for z in tqdm(range(len(folder_list))):
    os.chdir(folder_list[z])
    try:
        os.mkdir('check')
        os.mkdir('plots')
        os.mkdir('stats')
    except:
        print('Folder already exists')
        
    rois=glob.glob(folder_list[z]+'/video/*.roi')
    

# Liste mit benutzerdefinierter Sortierfunktion sortieren
    rois.sort(key=extract_numbers)
    
    
    centroid_y=np.zeros((len(rois)))
    centroid_x=np.zeros((len(rois)))
    
    for y in range(len(rois)):
        roi = read_roi_file(rois[y])
        keys=list(roi.keys())
        roi=roi[keys[0]]
        centroid_y[y]=np.mean(roi['y'])
        centroid_x[y]=np.mean(roi['x'])
        
    distance_matrix=np.zeros((len(centroid_y),len(centroid_y)))
    
    for l in range(len(centroid_y)):
        for k in range(len(centroid_y)):
            distance_matrix[l,k]=distance(centroid_x[l],centroid_y[l],centroid_x[k],centroid_y[k])
        
    df=pd.read_excel('video/results/manual/time_series.xlsx',sheet_name=None,engine='openpyxl')

    
    a=df['detrended']
    
    header=list(a.columns.values)
    v=0
    peaks_all=np.zeros((len(header),1))
    j=0
    
    
    d=np.zeros((len(header),len(a)))
    n_t=np.zeros((len(header),len(a)))
    header_active=np.zeros((len(header),))
    
    for i in header:
        
        zscore=stats.zscore(a[i])
        not_zscore=zscore
        kernel_size = 5
        kernel = np.ones(kernel_size) / kernel_size
        zscore = np.convolve(zscore, kernel, mode='same')
        
        #plt.figure()
        #plt.plot(zscore)
        peaks=np.where(zscore>=2)
        peaks=peaks[0]
        
        
        small_trace=a[i]
        
        small_trace[peaks]=np.nan
        small_trace[peaks+2]=np.nan
        small_trace[peaks-2]=np.nan
        
        
        small_trace[zscore>=2]=np.nan
        kernel_size = 10
        kernel = np.ones(kernel_size) / kernel_size
        #small_trace = np.convolve(small_trace, kernel, mode='same')
        small_trace=stats.zscore(small_trace, nan_policy='omit')
        
        
        small_peaks=np.where(small_trace>=3)
        peak_diff=np.diff(peaks)
        
        binary=np.zeros((len(zscore),1))
        
        normal_trace=np.zeros((len(zscore),1))
        binary[peaks]=1
        binary[small_peaks]=1
        binary=np.reshape(binary,(len(zscore),))
        kernel_size = 10
        kernel = np.ones(kernel_size) / kernel_size
        binary = np.convolve(binary, kernel, mode='same')
        binary[binary>0]=1

        
        if len(np.where(np.diff(binary)==1)[0])>=0:
            header_active[j]=1
            
        list_active.append(header_active)
        normal_trace=np.reshape(normal_trace,(len(a),))
        d[j]=binary
        n_t[j]=zscore
        j=j+1
        
        v=v+1
        thr=0
        
    d=np.array(d)
    d=np.transpose(d)
    n_t=np.array(n_t) 
    n_t=np.transpose(n_t) 
    
    high_activity_binary = np.zeros((d.shape[0],d.shape[1]))
    low_activity_binary = np.zeros((d.shape[0],d.shape[1]))
    
    high_activity_trace = np.zeros((d.shape[0],d.shape[1]))
    low_activity_trace = np.zeros((d.shape[0],d.shape[1]))
    
    for col in range(d.shape[1]):
        if header_active[col]==1:
            high_activity_binary[:,col] = d[:,col]
            high_activity_trace[:,col]=n_t[:,col]
        else:
            low_activity_binary[:,col] = d[:,col]
            low_activity_trace[:,col]=n_t[:,col]
            
    result_high_binary=zero_sum_cols(high_activity_binary)
    result_low_binary=zero_sum_cols(low_activity_binary)
         
    high_activity_binary=np.delete(high_activity_binary,result_high_binary,1)   
    low_activity_binary=np.delete(low_activity_binary,result_low_binary,1) 
    
    result_high_trace=zero_sum_cols(high_activity_trace)
    result_low_trace=zero_sum_cols(low_activity_trace)
         
    high_activity_trace=np.delete(high_activity_trace,result_high_trace,1)   
    low_activity_trace=np.delete(low_activity_trace,result_low_trace,1) 
    
    names=np.array(a.columns)
    header_active=header_active.astype(int)
    
    high_activity_trace=pd.DataFrame(high_activity_trace)
    high_activity_trace.columns=names[header_active==1]
    high_activity_binary=pd.DataFrame(high_activity_binary)
    high_activity_binary.columns=names[header_active==1]
    
    low_activity_trace=pd.DataFrame(low_activity_trace)
    low_activity_trace.columns=names[header_active==0]
    low_activity_binary=pd.DataFrame(low_activity_binary)
    low_activity_binary.columns=names[header_active==0]
    
    for y in range(len(rois)):
        roi = read_roi_file(rois[y])
        keys=list(roi.keys())
        roi=roi[keys[0]]
        centroid_y[y]=np.mean(roi['y'])
        centroid_x[y]=np.mean(roi['x'])
        
    high_centroid_y=centroid_y[header_active==1]
    high_centroid_x=centroid_x[header_active==1]
    high_distance_matrix=np.zeros((len(high_centroid_y),len(high_centroid_y)))
    
    for l in range(len(high_centroid_y)):
        for k in range(len(high_centroid_y)):
            high_distance_matrix[l,k]=distance(high_centroid_x[l],high_centroid_y[l],high_centroid_x[k],high_centroid_y[k])
            
    all_high_distance.append(np.concatenate(high_distance_matrix))
    
    
    
    names_high=list(high_activity_trace.columns)
    names_low=list(low_activity_trace.columns)
    
    high_activity_trace=np.array(high_activity_trace)
    high_activity_binary=np.array(high_activity_binary)
    
    ###high activity###
    corr_matrx=np.zeros((len(names_high),len(names_high)))

    index_max_corr=np.zeros((len(names_high),len(names_high)))
    b=np.zeros((maximum_lag*2,1))
    
    if len(names_high)>1:
        for i in range(len(names_high)):
            for k in range(len(names_high)):
                trace_1=(high_activity_binary[:,i])
                trace_2=(high_activity_binary[:,k])
                trace_1=pd.DataFrame(trace_1)
                trace_2=pd.DataFrame(trace_2)
                trace_1=trace_1.squeeze()
                trace_2=trace_2.squeeze()
                n_trace_1=(high_activity_trace[:,i])
                n_trace_2=(high_activity_trace[:,k])
                n_trace_1=pd.DataFrame(n_trace_1)
                n_trace_2=pd.DataFrame(n_trace_2)
                n_trace_1=n_trace_1.squeeze()
                n_trace_2=n_trace_2.squeeze()
                for n in range(maximum_lag):
                    p=n
                    b[n]=crosscorr(trace_1,trace_2, lag=p)
                    corr_matrx[i,k]=np.nanmax(b)
                    
                    index_max_corr[i,k]=(np.where(b==np.nanmax(b))[0][0])
                    
                
                    
                    
                if corr_matrx[i,k]>=0.5:
                    if np.sum(trace_1-trace_2)!=0:
                        plt.figure()
                        plt.subplot(411)
                        plt.plot(trace_1)
                        plt.xticks([])
                        plt.title(str(folder_list[z])+' '+str(names[i])+' '+str(names[k]))
                        plt.subplot(413)
                        plt.xticks([])
                        plt.plot(trace_2)
                        plt.subplot(412)
                        plt.xticks([])
                        plt.plot(n_trace_1)
                        plt.subplot(414)
                        plt.plot(n_trace_2)
                        
                        
                        plt.savefig(folder_list[z]+'\check\\'+'high_Unit '+str(i)+' and Unit '+str(k)+'.svg')
                        plt.show()
                        
        plt.figure()
        plt.imshow(corr_matrx,cmap='gray_r')
        plt.xticks(np.arange(len(names_high)), labels=names_high)
        plt.yticks(np.arange(len(names_high)), labels=names_high)
        plt.colorbar()
        plt.savefig(folder_list[z]+'\\plots\\corr_matrix.svg')
        df = pd.DataFrame(corr_matrx)
        df.to_excel(r'.\\stats\\correlations.xlsx', index=False)
        plt.figure()
        index_max_corr=np.abs(index_max_corr)
        plt.imshow(index_max_corr,cmap='gray_r')
        plt.xticks(np.arange(len(names_high)), labels=names_high)
        plt.yticks(np.arange(len(names_high)), labels=names_high)
        plt.colorbar()
        plt.savefig(folder_list[z]+'\\plots\\lag_matrix.svg')
        df = pd.DataFrame(index_max_corr)
        df.to_excel(r'.\\stats\\lag.xlsx', index=False)
                        
        index_max_corr=np.abs(index_max_corr)
        all_cross_corr.append(np.concatenate(corr_matrx,axis=0))
        all_index.append(np.concatenate(index_max_corr,axis=0))
            
all_high_distance=np.concatenate(all_high_distance)           
all_cross_corr=np.concatenate(all_cross_corr)
all_index=np.concatenate(all_index)
all_index=all_index[all_cross_corr<.99]
all_high_distance=all_high_distance[all_cross_corr<.99]
all_cross_corr=all_cross_corr[all_cross_corr<.99]
r_square=all_cross_corr**2

list_active=np.concatenate(list_active)

all_high_cross_corr=all_cross_corr
all_high_index=all_index

plt.figure(dpi=300)
plt.title(str(len(r_square[r_square>0.1])*100/len(r_square))[:4]+' % with r_square >0.1; count='+str(int(len(r_square)/2))+' pairs of units',fontsize=12) 
sns.histplot(r_square[r_square>0.1], stat='probability')


plt.ylabel('Percent of units [%]')
plt.xlabel('r_square')
sns.despine()

plt.savefig(path+'/R_square_high_active_histogram.svg')


plt.figure(dpi=300)
sns.histplot(all_index[r_square>0.1], stat='probability')
plt.title(str(len(r_square[r_square>0.1])*100/len(r_square))[:4]+' % with r_square >0.1;count='+str(int(len(r_square)/2))+' pairs of units',fontsize=12)


plt.ylabel('Percent of units [%]')
plt.xlabel('Lag')
sns.despine()

plt.savefig(path+'/Lag_high_active_histogram.svg')

plt.figure()
plt.plot(all_high_distance,all_high_cross_corr,'r.')
#plt.plot(all_low_distance,all_low_cross_corr,'b.')
plt.xlabel('Distance [Pixel]')
plt.ylabel('Cross correlation')
sns.despine()

plt.savefig(path+'/corr_distance_plot.svg')

plt.figure()
plt.plot(all_high_distance,all_index,'r.')
#plt.plot(all_low_distance,all_low_cross_corr,'b.')
plt.xlabel('Distance [Pixel]')
plt.ylabel('Lag')
sns.despine()

plt.savefig(path+'/lag_distance_plot.svg')

plt.figure()
plt.plot(all_index,all_high_cross_corr,'r.')
#plt.plot(all_low_distance,all_low_cross_corr,'b.')
plt.xlabel('Lag')
plt.ylabel('cross corr')
sns.despine()

plt.savefig(path+'/lag_crosscorr_plot.svg')

